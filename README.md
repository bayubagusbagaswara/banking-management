# Bank Management menggunakan JPA

- Buat API dari quiz 2 Hibernate, tapi ini menggunakan JPA
- Kita menambahkan layer service dan controller
- Buat Entity dan Repository

## Entity

- entity yang kita buat tetap sama
- perhatikan pula relasi antar entitas atau table
- ada entitas Nasabah, Wilayah, dan Transaksi
- Wilayah, terdiri dari: Provinsi, Kota, Kecamatan, Kelurahan
- Nasabah, terdiri dari: Nasabah Badan Usaha dan Nasabah Perorangan 
- Transaksi/Bank, terdiri dari: ProdukTabungan, RekeningTabungan, dan TransaksiTabungan 

## Testing Wilayah
- Buat service untuk Provinsi, Kota, Kecamatan, dan Kelurahan
- Buat sample datanya di file sql, migrate datanya
- Lalu testing per table
- Saat kita mendapatkan data Kelurahan, maka kita bisa mendapatkan data Kecamatan juga didalamnya

## Response Get Provinsi By ID

```json
{
  "id": "jatim",
  "code": "12",
  "name": "Jawa Timur"
}
```
## Response Get Kota By ID
```json
{
  "id": "kediri",
  "code": "123",
  "name": "Kota Kediri",
  "provinsi": {
    "id": "jatim",
    "code": "12",
    "name": "Jawa Timur"
  }
}
```
## Response Get Kecamatan By ID

```json
{
  "id": "pesantren",
  "code": "1234",
  "name": "Kecamatan Pesantren",
  "kota": {
    "id": "kediri",
    "code": "123",
    "name": "Kota Kediri"
  }
}
```

## Response Get Kelurahan By ID

```json
{
  "id": "singonegaran",
  "code": "12345",
  "name": "Singonegaran",
  "kecamatan": {
    "id": "pesantren",
    "code": "1234",
    "name": "Kecamatan Pesantren"
  }
}
```