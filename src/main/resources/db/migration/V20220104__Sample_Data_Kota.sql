-- KOTA DI PROVINSI JAKARTA
insert into kota (id, name, code, created_by, created_at, status_record, id_provinsi)
values ('3171', 'KOTA ADM. JAKARTA PUSAT', '3171', 'Current User', current_timestamp, 'ACTIVE', '31'),
        ('3172', 'KOTA ADM. JAKARTA UTARA', '3172','Current User', current_timestamp, 'ACTIVE', '31'),
        ('3173', 'KOTA ADM. JAKARTA BARAT', '3173', 'Current User', current_timestamp, 'ACTIVE', '31'),
        ('3174', 'KOTA ADM. JAKARTA SELATAN', '3174', 'Current User', current_timestamp, 'ACTIVE', '31'),
        ('3175', 'KOTA ADM. JAKARTA TIMUR', '3175', 'Current User', current_timestamp, 'ACTIVE', '31');

-- KOTA DI PROVINSI JAWA BARAT
insert into kota (id, name, code, created_by, created_at, status_record, id_provinsi)
values ('3204', 'KAB. BANDUNG', '3204', 'Current User', current_timestamp, 'ACTIVE', '32'),
        ('3273', 'KOTA BANDUNG', '3273', 'Current User', current_timestamp, 'ACTIVE', '32'),
        ('3275', 'KOTA BEKASI', '3275', 'Current User', current_timestamp, 'ACTIVE', '32');

-- KOTA DI PROVINSI JAWA TENGAH
insert into kota (id, name, code, created_by, created_at, status_record, id_provinsi)
values ('3306', 'KAB. PURWOREJO', '3306', 'Current User', current_timestamp, 'ACTIVE', '33'),
        ('3372', 'KOTA SURAKARTA', '3372', 'Current User', current_timestamp, 'ACTIVE', '33');

-- KOTA DI PROVINSI YOGYAKARTA
insert into kota (id, name, code, created_by, created_at, status_record, id_provinsi)
values ('3404', 'KAB. SLEMAN', '3404', 'Current User', current_timestamp, 'ACTIVE', '34'),
        ('3471', 'KOTA YOGYAKARTA', '3471', 'Current User', current_timestamp, 'ACTIVE', '34');

-- KOTA DI PROVINSI JAWA TIMUR
insert into kota (id, name, code, created_by, created_at, status_record, id_provinsi)
values ('3517', 'KAB. JOMBANG', '3517', 'Current User', current_timestamp, 'ACTIVE', '35'),
        ('3571', 'KOTA KEDIRI', '3571', 'Current User', current_timestamp, 'ACTIVE', '35'),
        ('3578', 'KOTA SURABAYA', '3578', 'Current User', current_timestamp, 'ACTIVE', '35');

-- KOTA DI PROVINSI BANTEN
INSERT INTO kota (id, name, code, created_by, created_at, status_record, id_provinsi)
VALUES ('3671', 'KOTA TANGERANG', '3671', 'Current User', current_timestamp, 'ACTIVE', '36'),
        ('3674', 'KOTA TANGERANG SELATAN', '3674', 'Current User', current_timestamp, 'ACTIVE', '36');

-- KOTA DI PROVINSI BALI
INSERT INTO kota ( id, name, code, created_by, created_at, status_record, id_provinsi)
VALUES ('5103', 'KAB. BADUNG', '5103', 'Current User', current_timestamp, 'ACTIVE', '51'),
        ('5104', 'KAB. GIANYAR', '5104', 'Current User', current_timestamp, 'ACTIVE', '51'),
        ('5171', 'KOTA DENPASAR', '5171', 'Current User', current_timestamp, 'ACTIVE', '51');
