-- PROVINSI
create table provinsi (
    id character varying(64) primary key default uuid_generate_v4(),
    code character varying(20) not null unique,
    name character varying(50) not null,
    created_at timestamp not null default now(),
    created_by character varying(100) not null,
    updated_at timestamp,
    updated_by character varying(100),
    status_record character varying(255) not null
);

-- KOTA
create table kota (
    id character varying(64) primary key default uuid_generate_v4(),
    name character varying(50) not null,
    code character varying(20) not null unique,
    created_at timestamp not null default now(),
    created_by character varying(100) not null,
    updated_at timestamp,
    updated_by character varying(100),
    status_record character varying(255) not null,
    id_provinsi character varying(64),
    constraint fk_kota_provinsi foreign key (id_provinsi) references provinsi(id) on update cascade on delete cascade
);

-- KECAMATAN
create table kecamatan (
    id character varying(64) primary key default uuid_generate_v4(),
    name character varying(50) not null,
    code character varying(20) not null unique,
    created_at timestamp not null default now(),
    created_by character varying(100) not null,
    updated_at timestamp,
    updated_by character varying(100),
    status_record character varying(255) not null,
    id_kota character varying(64),
    constraint fk_kecamatan_kota foreign key (id_kota) references kota(id) on update cascade on delete cascade
);

-- KELURAHAN
create table kelurahan (
    id character varying(64) primary key default uuid_generate_v4(),
    name character varying(50) not null,
    code character varying(20) not null unique,
    created_at timestamp not null default now(),
    created_by character varying(100) not null,
    updated_at timestamp,
    updated_by character varying(100),
    status_record character varying(255) not null,
    id_kecamatan character varying(64),
    constraint fk_kelurahan_kecamatan foreign key (id_kecamatan) references kecamatan(id) on update cascade on delete cascade
);
