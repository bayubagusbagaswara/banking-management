-- PRODUK TABUNGAN
create table produk_tabungan (
    id character varying(64) primary key default uuid_generate_v4(),
    nama character varying(255),
    min_setoran_awal numeric(19,2),
    biaya_admin numeric(19,2),
    suku_bunga real,
    created_at timestamp not null default now(),
    created_by character varying(100) not null,
    updated_at timestamp,
    updated_by character varying(255),
    status_record character varying(255) not null
);

-- REKENING TABUNGAN
create table rekening_tabungan (
    id character varying(64) primary key default uuid_generate_v4(),
    biaya_admin numeric(19,2),
    saldo numeric(19,2),
    suku_bunga real,
    created_at timestamp not null default now(),
    created_by character varying(100) not null,
    updated_at timestamp,
    updated_by character varying(100),
    status_record character varying(255) not null,
    id_nasabah character varying(64),
    id_produk_tabungan character varying(64),
    constraint fk_rekening_tabungan_nasabah foreign key (id_nasabah) references nasabah(id),
    constraint fk_rekening_tabungan_produk_tabungan foreign key (id_produk_tabungan) references produk_tabungan(id)
);

-- TRANSAKSI TABUNGAN
create table transaksi_tabungan (
    id character varying(64) primary key default uuid_generate_v4(),
    debit numeric(19,2),
    kredit numeric(19,2),
    saldo numeric(19,2),
    tanggal timestamp,
    created_at timestamp not null default now(),
    created_by character varying(255) not null,
    updated_at timestamp,
    updated_by character varying(255),
    status_record character varying(255) not null,
    id_rekening_tabungan character varying(64),
    constraint fk_transaksi_tabungan_rekening_tabungan foreign key (id_rekening_tabungan) references rekening_tabungan(id)
);

