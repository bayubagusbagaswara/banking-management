-- KECAMATAN DI KOTA JAKARTA PUSAT, PROVINSI JAKARTA
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('317103', 'Kemayoran', '317103', 'Current User', current_timestamp, 'ACTIVE', '3171'),
        ('317106', 'Menteng', '317106', 'Current User', current_timestamp, 'ACTIVE', '3171');

-- KECAMATAN DI JAKARTA UTARA, PROVINSI JAKARTA
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('317202', 'Tanjung Priok', '317202', 'Current User', current_timestamp, 'ACTIVE', '3172');

-- KECAMATAN DI JAKARTA BARAT, PROVINSI JAKARTA
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('317301', 'Cengkareng', '317301', 'Current User', current_timestamp, 'ACTIVE', '3173');

-- KECAMATAN DI JAKARTA SELATAN, PROVINSI JAKARTA
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('317401', 'Tebet', '317401', 'Current User', current_timestamp, 'ACTIVE', '3174'),
        ('317407', 'Kebayoran Baru', '317407', 'Current User', current_timestamp, 'ACTIVE', '3174');

-- KECAMATAN DI JAKARTA TIMUR, PROVINSI JAKARTA
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('317503', 'Jatinegara', '317503', 'Current User', current_timestamp, 'ACTIVE', '3175');

-- KECAMATAN DI KAB BANDUNG, PROVINSI JAWA BARAT
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('320405', 'Cileunyi', '320405', 'Current User', current_timestamp, 'ACTIVE', '3204'),
        ('320428', 'Rancaekek', '320428', 'Current User', current_timestamp, 'ACTIVE', '3204');

-- KECAMATAN DI KOTA BANDUNG, PROVINSI JAWA BARAT
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('327325', 'Cibiru', '327325', 'Current User', current_timestamp, 'ACTIVE', '3273'),
        ('327328', 'Panyileukan', '327328', 'Current User', current_timestamp, 'ACTIVE', '3273');

-- KECAMATAN DI KOTA BEKASI, PROVINSI JAWA BARAT
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('327509', 'Jatiasih', '327509', 'Current User', current_timestamp, 'ACTIVE', '3275');

-- KECAMATAN DI KAB PURWOREJO, PROVINSI JAWA TENGAH
INSERT INTO kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
VALUES ('330606', 'Purworejo', '330606', 'Current User', current_timestamp, 'ACTIVE', '3306');

-- KECAMATAN DI KOTA SURAKARTA, PROVINSI JAWA TENGAH
insert into kecamatan(id, name, code, created_by, created_at, status_record, id_kota)
values ('337203', 'Pasar Kliwon', '337203', 'Current User', current_timestamp, 'ACTIVE', '3372'),
        ('337204', 'Jebres', '337204', 'Current User', current_timestamp, 'ACTIVE', '3372');

-- KECAMATAN DI KAB SLEMAN, PROVINSI YOGYAKARTA
insert into kecamatan(id, name, code, created_by, created_at, status_record, id_kota)
values ('340413', 'Sleman', '340413', 'Current User', current_timestamp, 'ACTIVE', '3404');

-- KECAMATAN DI KOTA YOGYAKARTA, PROVINSI YOGYAKARTA
insert into kecamatan(id, name, code, created_by, created_at, status_record, id_kota)
values ('347103', 'Gondokusuman', '347103', 'Current User', current_timestamp, 'ACTIVE', '3471'),
        ('347114', 'Kotagede', '347114', 'Current User', current_timestamp, 'ACTIVE', '3471');

-- KECAMATAN DI KAB JOMBANG, PROVINSI JAWA TIMUR
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('351701', 'Perak', '351701', 'Current User', current_timestamp, 'ACTIVE', '3517'),
        ('351709', 'Jombang', '351709', 'Current User', current_timestamp, 'ACTIVE', '3517');

-- KECAMATAN DI KOTA KEDIRI, PROVINSI JAWA TIMUR
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('357101', 'Mojoroto', '357101', 'Current User', current_timestamp, 'ACTIVE', '3517'),
        ('357102', 'Kota', '357102', 'Current User', current_timestamp, 'ACTIVE', '3517'),
        ('357103', 'Pesantren', '357103', 'Current User', current_timestamp, 'ACTIVE', '3517');

-- KECAMATAN DI KOTA SURABAYA, PROVINSI JAWA TIMUR
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('357808', 'Gubeng', '357808', 'Current User', current_timestamp, 'ACTIVE', '3578'),
        ('357809', 'Sukolilo', '357809', 'Current User', current_timestamp, 'ACTIVE', '3578'),
        ('357826', 'Mulyorejo', '357826', 'Current User', current_timestamp, 'ACTIVE', '3578');

-- KECAMATAN DI KOTA TANGERANG, PROVINSI BANTEN
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('367101', 'Tangerang', '367101', 'Current User', current_timestamp, 'ACTIVE', '3671'),
        ('367107', 'Karawaci', '367107', 'Current User', current_timestamp, 'ACTIVE', '3671');

-- KECAMATAN DI KOTA TANGERANG SELATAN, PROVINSI BANTEN
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('367401', 'Serpong', '367401', 'Current User', current_timestamp, 'ACTIVE', '3674');

-- KECAMATAN DI KAB BADUNG, PROVINSI BALI
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('510301', 'Kuta', '510301', 'Current User', current_timestamp, 'ACTIVE', '5103');

-- KECAMATAN DI KAB GIANYAR, PROVINSI BALI
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('510405', 'Ubud', '510405', 'Current User', current_timestamp, 'ACTIVE', '5104');

-- KECAMATAN DI KOTA DENPASAR, PROVINSI BALI
insert into kecamatan (id, name, code, created_by, created_at, status_record, id_kota)
values ('517101', 'Denpasar Selatan', '517101', 'Current User', current_timestamp, 'ACTIVE', '5171');