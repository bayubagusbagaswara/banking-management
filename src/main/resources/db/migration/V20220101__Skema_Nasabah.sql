-- NASABAH
create table nasabah (
    id character varying(64) primary key default uuid_generate_v4(),
    cif character varying(30) not null unique,
    nama_kepemilikan character varying(100) not null,
    nomor_identitas character varying(30) not null unique,
    created_at timestamp not null default now(),
    created_by character varying(255) not null,
    updated_at timestamp,
    updated_by character varying(255),
    status_record character varying(255) not null,
    id_kelurahan character varying(64),
    constraint fk_nasabah_kelurahan foreign key (id_kelurahan) references kelurahan (id)  on update cascade on delete cascade
);

-- NASABAH BADAN USAHA
create table nasabah_badan_usaha (
    nomor_akta_terakhir character varying(50) not null,
    nomor_siup character varying(50) not null,
    nomor_telephone character varying(30) not null,
    id_nasabah character varying(64) not null,
    constraint fk_nasabah_badan_usaha_nasabah foreign key (id_nasabah) references nasabah(id)  on update cascade on delete cascade
);

-- NASABAH PERORANGAN
create table nasabah_perorangan (
    nama_ibu_kandung character varying(100) not null,
    nomor_telephone character varying(30) not null,
    tanggal_lahir date not null,
    id_nasabah character varying(64) not null,
    constraint fk_nasabah_perorangan_nasabah foreign key (id_nasabah) references nasabah(id)  on update cascade on delete cascade
);
