insert into produk_tabungan (id, nama, min_setoran_awal, suku_bunga, biaya_admin, created_by, created_at, status_record)
values('mapan', 'Tabungan Mapan', 500000, 0.1, 1500, 'Current User', current_timestamp, 'ACTIVE'),
    ('tabplus', 'Tabungan Plus', 1000000, 0.2, 2500, 'Current User', current_timestamp, 'ACTIVE'),
    ('tabunganku', 'TabunganKu', 100000, 0, 0, 'Current User', current_timestamp, 'ACTIVE');