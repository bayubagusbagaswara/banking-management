-- KELURAHAN DI KECAMATAN KEMAYORAN, KOTA JAKARTA PUSAT, PROVINSI JAKARTA
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3171031001', 'Kemayoran', '3171031001', 'Current User', current_timestamp, 'ACTIVE', '317103'),
        ('3171031002', 'Kebon Kosong', '3171031002', 'Current User', current_timestamp, 'ACTIVE', '317103'),
        ('3171031006', 'Cempaka Baru', '3171031006', 'Current User', current_timestamp, 'ACTIVE', '317103');

-- KELURAHAN DI KECAMATAN MENTENG, KOTA JAKARTA PUSAT, PROVINSI JAKARTA
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3171061001', 'Menteng', '3171061001', 'Current User', current_timestamp, 'ACTIVE', '317106'),
        ('3171061003', 'Cikini', '3171061003', 'Current User', current_timestamp, 'ACTIVE', '317106'),
        ('3171061004', 'Gondangdia', '3171061004', 'Current User', current_timestamp, 'ACTIVE', '317106');

-- KELURAHAN DI KECAMATAN TEBET, KOTA JAKARTA SELATAN, PROVINSI JAKARTA
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3174011001', 'Tebet Timur', '3174011001', 'Current User', current_timestamp, 'ACTIVE', '317401'),
        ('3174011003', 'Menteng Dalam', '3174011003', 'Current User', current_timestamp, 'ACTIVE', '317401'),
        ('3174011007', 'Manggarai', '3174011007', 'Current User', current_timestamp, 'ACTIVE', '317401');

-- KELURAHAN DI KECAMATAN KEBAYORAN BARU, KOTA JAKARTA SELATAN, PROVINSI JAKARTA
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3174071001', 'Melawai', '3174071001', 'Current User', current_timestamp, 'ACTIVE', '317407'),
        ('3174071006', 'Senayan', '3174071006', 'Current User', current_timestamp, 'ACTIVE', '317407'),
        ('3174071009', 'Gandaria Utara', '3174071009', 'Current User', current_timestamp, 'ACTIVE', '317407');

-- KELURAHAN DI KECAMATAN JATINEGARA, KOTA JAKARTA TIMUR, PROVINSI JAKARTA
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3175031001', 'Kampung Melayu', '3175031001', 'Current User', current_timestamp, 'ACTIVE', '317503'),
    ('3175031002', 'Bidara Cina', '3175031002', 'Current User', current_timestamp, 'ACTIVE', '317503'),
    ('3175031007', 'Cipinang Besar Selatan', '3175031007', 'Current User', current_timestamp, 'ACTIVE', '317503'),
    ('3175031008', 'Cipinang Besar Utara', '3175031008', 'Current User', current_timestamp, 'ACTIVE', '317503');

-- KELURAHAN DI KECAMATAN CILEUNYI, KAB BANDUNG, PROVINSI JAWA BARAT
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3204052001', 'Cileunyi Kulon', '3204052001', 'Current User', current_timestamp, 'ACTIVE', '320405'),
        ('3204052004', 'Cinunuk', '3204052004', 'Current User', current_timestamp, 'ACTIVE', '320405');

-- KELURAHAN DI KECAMATAN CIBIRU, KOTA BANDUNG, PROVINSI JAWA BARAT
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3273251001', 'Palasari', '3273251001', 'Current User', current_timestamp, 'ACTIVE', '327325'),
        ('3273251002', 'Cipadung', '3273251002', 'Current User', current_timestamp, 'ACTIVE', '327325');

-- KELURAHAN DI KECAMATAN JATIASIH, KOTA BEKASI, PROVINSI JAWA BARAT
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3275091001', 'Jatimekar', '3275091001', 'Current User', current_timestamp, 'ACTIVE', '327509'),
        ('3275091002', 'Jatiasih', '3275091002', 'Current User', current_timestamp, 'ACTIVE', '327509');

-- KELURAHAN DI KECAMATAN PURWOREJO, KAB PURWOREJO, PROVINSI JAWA TENGAH
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3306061010', 'Tambakrejo', '3306061010', 'Current User', current_timestamp, 'ACTIVE', '330606'),
        ('3306061017', 'Purworejo', '3306061017', 'Current User', current_timestamp, 'ACTIVE', '330606');

-- KELURAHAN DI KECAMATAN PASAR KLIWON, KOTA SURAKARTA, PROVINSI JAWA TENGAH
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3372031003', 'Pasarkliwon', '3372031003', 'Current User', current_timestamp, 'ACTIVE', '337203'),
        ('3372031009', 'Kauman', '3372031009', 'Current User', current_timestamp, 'ACTIVE', '337203');

-- KELURAHAN DI KECAMATAN KOTA GEDE, KOTA YOGYAKARTA, PROVINSI YOGYAKARTA
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3471141001', 'Rejowinangun', '3471141001', 'Current User', current_timestamp, 'ACTIVE', '347114'),
        ('3471141003', 'Purbayan', '3471141003', 'Current User', current_timestamp, 'ACTIVE', '347114');

-- KELURAHAN DI KECAMATAN PERAK, KAB JOMBANG, PROVINSI JAWA TIMUR
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3517012005', 'Perak', '3517012005', 'Current User', current_timestamp, 'ACTIVE', '351701'),
        ('3517012006', 'Sembung', '3517012006', 'Current User', current_timestamp, 'ACTIVE', '351701'),
        ('3517012013', 'Cangkringrandu', '3517012013', 'Current User', current_timestamp, 'ACTIVE', '351701');

-- KELURAHAN DI KECAMATAN JOMBANG, KAB JOMBANG, PROVINSI JAWA TIMUR
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3517092012', 'Jombang', '3517092012', 'Current User', current_timestamp, 'ACTIVE', '351709');

-- KELURAHAN DI KECAMATAN KOTA, KOTA KEDIRI, PROVINSI JAWA TIMUR
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3571021002', 'Balowerti', '3571021002', 'Current User', current_timestamp, 'ACTIVE', '357102'),
        ('3571021013', 'Kaliombo', '3571021013', 'Current User', current_timestamp, 'ACTIVE', '357102'),
        ('3571021014', 'Ngronggo', '3571021014', 'Current User', current_timestamp, 'ACTIVE', '357102');

-- KELURAHAN DI KECAMATAN PESANTREN, KOTA KEDIRI, PROVINSI JAWA TIMUR
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3571031005', 'Singonegaran', '3571031005', 'Current User', current_timestamp, 'ACTIVE', '357103'),
        ('3571031013', 'Pesantren', '3571031013', 'Current User', current_timestamp, 'ACTIVE', '357103');

-- KELURAHAN DI KECAMATAN SUKOLILO, KOTA SURABAYA, PROVINSI JAWA TIMUR
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3578091001', 'Keputih', '3578091001', 'Current User', current_timestamp, 'ACTIVE', '357809'),
        ('3578091002', 'Gebang Putih', '3578091002', 'Current User', current_timestamp, 'ACTIVE', '357809');

-- KELURAHAN DI KECAMATAN MULYOREJO, KOTA SURABAYA, PROVINSI JAWA TIMUR
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('3578261001', 'Mulyorejo', '3578261001', 'Current User', current_timestamp, 'ACTIVE', '357826'),
        ('3578261002', 'Manyar Sabrangan', '3578261002', 'Current User', current_timestamp, 'ACTIVE', '357826');

-- KELURAHAN DI KECAMATAN KUTA, KAB BADUNG, PROVINSI BALI
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('5103011002', 'Kuta', '5103011002', 'Current User', current_timestamp, 'ACTIVE', '510301'),
        ('5103011004', 'Legian', '5103011004', 'Current User', current_timestamp, 'ACTIVE', '510301'),
        ('5103011005', 'Seminyak', '5103011005', 'Current User', current_timestamp, 'ACTIVE', '510301');

-- KELURAHAN DI KECAMATAN UBUD, KAB GIANYAR, PROVINSI BALI
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('5104051005', 'Ubud', '5104051005', 'Current User', current_timestamp, 'ACTIVE', '510405'),
        ('5104052006', 'Peliatan', '5104052006', 'Current User', current_timestamp, 'ACTIVE', '510405');

-- KELURAHAN DI KECAMATAN DENPASAR SELATAN, KOTA DENPASAR, PROVINSI BALI
insert into kelurahan (id, name, code, created_by, created_at, status_record, id_kecamatan)
values ('5171011002', 'Pedungan', '5171011002', 'Current User', current_timestamp, 'ACTIVE', '517101'),
        ('5171011006', 'Sanur', '5171011006', 'Current User', current_timestamp, 'ACTIVE', '517101');