package com.banking.management.dto.nasabah;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateNasabahBadanUsahaRequest {

    @NotBlank(message = "Nama Kepemilikan can not blank")
    private String namaKepemilikan;

    @NotBlank(message = "CIF can not blank")
    private String cif;

    @NotBlank(message = "Nomor Identitas can not blank")
    private String noIdentitas;

    @NotBlank(message = "Kelurahan ID can not blank")
    private String kelurahanId;

    @NotBlank(message = "Nomor Siup can not blank")
    private String noSiup;

    @NotBlank(message = "Nomor Akta Terakhir can not blank")
    private String noAktaTerakhir;

    @NotBlank(message = "Nomor Telephone can not blank")
    private String noTelp;
}
