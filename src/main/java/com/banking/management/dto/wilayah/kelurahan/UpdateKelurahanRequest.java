package com.banking.management.dto.wilayah.kelurahan;

import com.banking.management.entity.wilayah.Kecamatan;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateKelurahanRequest {

    @NotBlank(message = "Name must not be blank")
    private String name;

    @NotBlank(message = "Code must not be blank")
    private String code;

    @NotNull(message = "Kecamatan can not null")
    private Kecamatan kecamatan;
}
