package com.banking.management.repository.wilayah;

import com.banking.management.entity.wilayah.Kota;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface KotaRepository extends JpaRepository<Kota, String> {

    Optional<Kota> findByNameIgnoreCase(String name);
    Optional<Kota> findByCode(String code);
    List<Kota> findByNameContainingIgnoreCase(String name);
    List<Kota> findByProvinsiId(String provinsiId);
}
