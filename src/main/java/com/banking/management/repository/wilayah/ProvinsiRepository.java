package com.banking.management.repository.wilayah;

import com.banking.management.entity.wilayah.Provinsi;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProvinsiRepository extends JpaRepository<Provinsi, String> {

    Optional<Provinsi> findByNameIgnoreCase(String name);
    Optional<Provinsi> findByCode(String code);
    List<Provinsi> findByNameContainingIgnoreCase(String name);
}
