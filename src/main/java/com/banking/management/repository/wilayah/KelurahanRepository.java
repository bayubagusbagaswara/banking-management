package com.banking.management.repository.wilayah;

import com.banking.management.entity.wilayah.Kelurahan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface KelurahanRepository extends JpaRepository<Kelurahan, String> {

    Optional<Kelurahan> findByNameIgnoreCase(String name);
    Optional<Kelurahan> findByCode(String code);
    List<Kelurahan> findByNameContainingIgnoreCase(String name);
    List<Kelurahan> findByKecamatanId(String kecamatanId);
}
