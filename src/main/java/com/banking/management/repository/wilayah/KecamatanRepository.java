package com.banking.management.repository.wilayah;

import com.banking.management.entity.wilayah.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface KecamatanRepository extends JpaRepository<Kecamatan, String> {

    Optional<Kecamatan> findByNameIgnoreCase(String name);
    Optional<Kecamatan> findByCode(String code);
    List<Kecamatan> findByNameContainingIgnoreCase(String name);
    List<Kecamatan> findByKotaId(String kotaId);
}
