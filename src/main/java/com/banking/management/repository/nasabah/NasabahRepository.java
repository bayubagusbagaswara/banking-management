package com.banking.management.repository.nasabah;

import com.banking.management.entity.nasabah.Nasabah;
import com.banking.management.entity.nasabah.NasabahBadanUsaha;
import com.banking.management.entity.nasabah.NasabahPerorangan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NasabahRepository extends JpaRepository<Nasabah, String> {

    Optional<NasabahPerorangan> findNasabahPeroranganById(String value);
    Optional<NasabahBadanUsaha> findNasabahBadanUsahaById(String value);
}
