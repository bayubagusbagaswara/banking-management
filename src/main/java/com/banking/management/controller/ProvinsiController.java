package com.banking.management.controller;

import com.banking.management.dto.WebResponse;
import com.banking.management.dto.wilayah.provinsi.*;
import com.banking.management.exception.ProvinsiNotFoundException;
import com.banking.management.service.wilayah.ProvinsiService;
import com.banking.management.utils.AppConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/provinsi")
public class ProvinsiController {

    private final ProvinsiService provinsiService;

    public ProvinsiController(ProvinsiService provinsiService) {
        this.provinsiService = provinsiService;
    }

    // create Provinsi
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetProvinsiResponse> createProvinsi(@RequestBody CreateProvinsiRequest createProvinsiRequest) {
        GetProvinsiResponse getProvinsiResponse = provinsiService.createProvinsi(createProvinsiRequest);
        return WebResponse.<GetProvinsiResponse>builder()
                .code(HttpStatus.CREATED.value())
                .status(HttpStatus.CREATED)
                .data(getProvinsiResponse)
                .build();
    }

    // get Provinsi By Id
    @GetMapping(value = "/{idProvinsi}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetProvinsiResponse> getProvinsiById(@PathVariable("idProvinsi") String provinsiId) throws ProvinsiNotFoundException {
        GetProvinsiResponse getProvinsiResponse = provinsiService.getProvinsiById(provinsiId);
        return WebResponse.<GetProvinsiResponse>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .data(getProvinsiResponse)
                .build();
    }

    // update Provinsi
    @PutMapping(value = "/{idProduct}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetProvinsiResponse> updateProvinsi(@PathVariable("idProduct") String id, @RequestBody UpdateProvinsiRequest updateProductRequest) throws ProvinsiNotFoundException {
        GetProvinsiResponse getProvinsiResponse = provinsiService.updateProvinsi(id, updateProductRequest);
        return WebResponse.<GetProvinsiResponse>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .data(getProvinsiResponse)
                .build();
    }

    // delete Provinsi By Id
    @DeleteMapping(value = "/{idProduct}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<String> deleteProduct(@PathVariable("idProduct") String id) throws ProvinsiNotFoundException {
        provinsiService.deleteProvinsi(id);
        return WebResponse.<String>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .data(null)
                .build();
    }

    // get All Provinsi
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetAllProvinsiResponse> getAllProvinsi(
            @RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) Integer pageNo,
            @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {

        int pageSize = 5;
        GetAllProvinsiRequest getAllProvinsiRequest = new GetAllProvinsiRequest(pageNo, pageSize, sortBy, sortDir);
        GetAllProvinsiResponse getAllProvinsiResponse = provinsiService.getAllProvinsi(getAllProvinsiRequest);
        return WebResponse.<GetAllProvinsiResponse>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK)
                .data(getAllProvinsiResponse)
                .build();
    }
}
