package com.banking.management.service.wilayah;

import com.banking.management.dto.wilayah.kecamatan.*;
import com.banking.management.exception.KecamatanNotFoundException;
import com.banking.management.exception.KotaNotFoundException;

import java.util.List;

public interface KecamatanService {

    GetKecamatanResponse createKecamatan(String kotaId, CreateKecamatanRequest createKecamatanRequest) throws KotaNotFoundException;

    GetKecamatanResponse getKecamatanById(String kecamatanId) throws KecamatanNotFoundException;

    GetKecamatanResponse updateKecamatan(String kecamatanId, UpdateKecamatanRequest updateKecamatanRequest) throws KotaNotFoundException, KecamatanNotFoundException;

    void deleteKecamatan(String kecamatanId) throws KecamatanNotFoundException;

    GetAllKecamatanResponse getAllKecamatan(GetAllKecamatanRequest getAllKecamatanRequest);

    GetKecamatanResponse getKecamatanByName(String name) throws KecamatanNotFoundException;

    GetKecamatanResponse getKecamatanByCode(String code) throws KecamatanNotFoundException;

    List<GetKecamatanResponse> getKecamatanByNameContaining(String name);

    List<GetKecamatanResponse> getKecamatanByKotaId(String kotaId);
}
