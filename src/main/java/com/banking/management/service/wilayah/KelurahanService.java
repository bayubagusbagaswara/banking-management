package com.banking.management.service.wilayah;

import com.banking.management.dto.wilayah.kelurahan.CreateKelurahanRequest;
import com.banking.management.dto.wilayah.kelurahan.GetKelurahanResponse;
import com.banking.management.dto.wilayah.kelurahan.UpdateKelurahanRequest;
import com.banking.management.exception.KecamatanNotFoundException;
import com.banking.management.exception.KelurahanNotFoundException;

import java.util.List;

public interface KelurahanService {

    GetKelurahanResponse createKelurahan(String idKecamatan, CreateKelurahanRequest createKelurahanRequest) throws KecamatanNotFoundException;

    GetKelurahanResponse findByKelurahanById(String kelurahanId) throws KelurahanNotFoundException;

    GetKelurahanResponse updateKelurahan(String kelurahanId, UpdateKelurahanRequest updateKelurahanRequest) throws KecamatanNotFoundException, KelurahanNotFoundException;

    void deleteKelurahan(String kelurahanId) throws KelurahanNotFoundException;

    GetKelurahanResponse getKelurahanByName(String name) throws KelurahanNotFoundException;

    GetKelurahanResponse getKelurahanByCode(String code) throws KelurahanNotFoundException;

    List<GetKelurahanResponse> getKelurahanByNameContaining(String name);

    List<GetKelurahanResponse> getKelurahanByKecamatanId(String kecamatanId);
}
