package com.banking.management.service.wilayah;

import com.banking.management.dto.wilayah.provinsi.*;
import com.banking.management.exception.ProvinsiNotFoundException;

import java.util.List;

public interface ProvinsiService {

    GetProvinsiResponse createProvinsi(CreateProvinsiRequest createProvinsiRequest);

    GetProvinsiResponse getProvinsiById(String provinsiId) throws ProvinsiNotFoundException;

    GetAllProvinsiResponse getAllProvinsi(GetAllProvinsiRequest getAllProvinsiRequest);

    GetProvinsiResponse updateProvinsi(String provinsiId, UpdateProvinsiRequest updateProvinsiRequest) throws  ProvinsiNotFoundException;

    void deleteProvinsi(String provinsiId) throws ProvinsiNotFoundException;

    GetProvinsiResponse getProvinsiByName(String name) throws ProvinsiNotFoundException;

    GetProvinsiResponse getProvinsiByCode(String code) throws ProvinsiNotFoundException;

    List<GetProvinsiResponse> getProvinsiByNameContaining(String name);
}
