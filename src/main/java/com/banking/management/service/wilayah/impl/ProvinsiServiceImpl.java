package com.banking.management.service.wilayah.impl;

import com.banking.management.dto.wilayah.provinsi.*;
import com.banking.management.entity.wilayah.Provinsi;
import com.banking.management.exception.ProvinsiNotFoundException;
import com.banking.management.repository.wilayah.ProvinsiRepository;
import com.banking.management.service.wilayah.ProvinsiService;
import com.banking.management.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProvinsiServiceImpl implements ProvinsiService {

    private final ProvinsiRepository provinsiRepository;
    private final ValidationUtil validationUtil;

    public ProvinsiServiceImpl(ProvinsiRepository provinsiRepository, ValidationUtil validationUtil) {
        this.provinsiRepository = provinsiRepository;
        this.validationUtil = validationUtil;
    }

    @Override
    public GetProvinsiResponse createProvinsi(CreateProvinsiRequest createProvinsiRequest) {
        validationUtil.validate(createProvinsiRequest);
        Provinsi provinsi = new Provinsi();
        provinsi.setName(createProvinsiRequest.getName());
        provinsi.setCode(createProvinsiRequest.getCode());
        provinsi.setCreatedAt(LocalDateTime.now());
        provinsiRepository.save(provinsi);
        return mapProvinsiToGetProvinsiResponse(provinsi);
    }

    @Override
    public GetProvinsiResponse getProvinsiById(String provinsiId) throws ProvinsiNotFoundException {
        Provinsi provinsi = provinsiRepository.findById(provinsiId).orElseThrow(() -> new ProvinsiNotFoundException("Provinsi ID = ["+provinsiId+"] Not Found"));
        return mapProvinsiToGetProvinsiResponse(provinsi);
    }

    @Override
    public GetAllProvinsiResponse getAllProvinsi(GetAllProvinsiRequest getAllProvinsiRequest) {
        Integer pageNo = getAllProvinsiRequest.getPageNo();
        Integer pageSize = getAllProvinsiRequest.getPageSize();
        String sortBy = getAllProvinsiRequest.getSortBy();
        String sortDir = getAllProvinsiRequest.getSortDir();

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<Provinsi> provinsis = provinsiRepository.findAll(pageable);
        List<Provinsi> provinsiList = provinsis.getContent();
        List<GetProvinsiResponse> getProvinsiResponses = mapProvinsiToGetProvinsiResponseList(provinsiList);

        GetAllProvinsiResponse getAllProvinsiResponse = new GetAllProvinsiResponse();
        getAllProvinsiResponse.setGetProvinsiResponses(getProvinsiResponses);
        getAllProvinsiResponse.setPageNo(provinsis.getNumber());
        getAllProvinsiResponse.setPageSize(provinsis.getSize());
        getAllProvinsiResponse.setTotalElements(provinsis.getTotalElements());
        getAllProvinsiResponse.setTotalPages(provinsis.getTotalPages());
        getAllProvinsiResponse.setLast(provinsis.isLast());
        return getAllProvinsiResponse;
    }

    @Override
    public GetProvinsiResponse updateProvinsi(String provinsiId, UpdateProvinsiRequest updateProvinsiRequest) throws ProvinsiNotFoundException {
        validationUtil.validate(updateProvinsiRequest);
        Provinsi provinsi = provinsiRepository.findById(provinsiId).orElseThrow(() -> new ProvinsiNotFoundException("Provinsi ID = ["+provinsiId+"] Not Found"));
        provinsi.setName(updateProvinsiRequest.getName());
        provinsi.setCode(updateProvinsiRequest.getCode());
        provinsi.setUpdatedAt(LocalDateTime.now());
        provinsiRepository.save(provinsi);
        return mapProvinsiToGetProvinsiResponse(provinsi);
    }

    @Override
    public void deleteProvinsi(String provinsiId) throws ProvinsiNotFoundException {
        Provinsi provinsi = provinsiRepository.findById(provinsiId).orElseThrow(() -> new ProvinsiNotFoundException("Provinsi ID : ["+provinsiId+"] Not Found"));
        provinsiRepository.delete(provinsi);
    }

    @Override
    public GetProvinsiResponse findByCode(String code) throws ProvinsiNotFoundException {
        Provinsi provinsi = provinsiRepository.findByCode(code).orElseThrow(() -> new ProvinsiNotFoundException("Provinsi Code : ["+code+"] Not Found"));
        return mapProvinsiToGetProvinsiResponse(provinsi);
    }

    @Override
    public GetProvinsiResponse findByName(String name) throws ProvinsiNotFoundException {
        Provinsi provinsi = provinsiRepository.findByNameIgnoreCase(name).orElseThrow(() -> new ProvinsiNotFoundException("Provinsi Name : ["+name+"] Not Found"));
        return mapProvinsiToGetProvinsiResponse(provinsi);
    }

    @Override
    public List<GetProvinsiResponse> findByNameContaining(String name) {
        List<Provinsi> provinsiList = provinsiRepository.findByNameContainingIgnoreCase(name);
        return mapProvinsiToGetProvinsiResponseList(provinsiList);
    }

    private List<GetProvinsiResponse> mapProvinsiToGetProvinsiResponseList(List<Provinsi> provinsiList) {
        return provinsiList.stream()
                 .map((provinsi) -> {
                     GetProvinsiResponse getProvinsiResponse = new GetProvinsiResponse();
                     getProvinsiResponse.setId(provinsi.getId());
                     getProvinsiResponse.setName(provinsi.getName());
                     getProvinsiResponse.setCode(provinsi.getCode());
                     getProvinsiResponse.setCreatedAt(provinsi.getCreatedAt());
                     getProvinsiResponse.setUpdatedAt(provinsi.getUpdatedAt());
                     return getProvinsiResponse;
                 })
                .collect(Collectors.toList())
                ;
    }

    private GetProvinsiResponse mapProvinsiToGetProvinsiResponse(Provinsi provinsi) {
        GetProvinsiResponse getProvinsiResponse = new GetProvinsiResponse();
        getProvinsiResponse.setId(provinsi.getId());
        getProvinsiResponse.setName(provinsi.getName());
        getProvinsiResponse.setCode(provinsi.getCode());
        getProvinsiResponse.setCreatedAt(provinsi.getCreatedAt());
        getProvinsiResponse.setUpdatedAt(provinsi.getUpdatedAt());
        return getProvinsiResponse;
    }
}
