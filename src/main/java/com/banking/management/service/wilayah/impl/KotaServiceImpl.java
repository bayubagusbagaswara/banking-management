package com.banking.management.service.wilayah.impl;

import com.banking.management.dto.wilayah.kota.*;
import com.banking.management.entity.wilayah.Kota;
import com.banking.management.entity.wilayah.Provinsi;
import com.banking.management.exception.KotaNotFoundException;
import com.banking.management.exception.ProvinsiNotFoundException;
import com.banking.management.repository.wilayah.KotaRepository;
import com.banking.management.repository.wilayah.ProvinsiRepository;
import com.banking.management.service.wilayah.KotaService;
import com.banking.management.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class KotaServiceImpl implements KotaService {

    private final KotaRepository kotaRepository;
    private final ProvinsiRepository provinsiRepository;
    private final ValidationUtil validationUtil;

    public KotaServiceImpl(KotaRepository kotaRepository, ProvinsiRepository provinsiRepository, ValidationUtil validationUtil) {
        this.kotaRepository = kotaRepository;
        this.provinsiRepository = provinsiRepository;
        this.validationUtil = validationUtil;
    }

    @Override
    public GetKotaResponse createKota(String provinsiId, CreateKotaRequest createKotaRequest) throws ProvinsiNotFoundException {
        validationUtil.validate(createKotaRequest);
        // ambil dulu data Provinsi berdasarkan id
        Provinsi provinsi = provinsiRepository.findById(createKotaRequest.getProvinsiId()).orElseThrow(() -> new ProvinsiNotFoundException("Provinsi ID : ["+createKotaRequest.getProvinsiId()+"] Not Found"));
        Kota kota = new Kota();
        kota.setName(createKotaRequest.getName());
        kota.setCode(createKotaRequest.getCode());
        kota.setProvinsi(provinsi);
        kota.setCreatedAt(LocalDateTime.now());

        kotaRepository.save(kota);
        return mapKotaToGetKotaResponse(kota);
    }

    @Override
    public GetKotaResponse getKotaById(String kotaId) throws KotaNotFoundException {
        Kota kota = kotaRepository.findById(kotaId).orElseThrow(() -> new KotaNotFoundException("Kota ID : ["+kotaId+"] Not Found"));
        return mapKotaToGetKotaResponse(kota);
    }

    @Override
    public GetKotaResponse updateKota(String kotaId, UpdateKotaRequest updateKotaRequest) throws KotaNotFoundException, ProvinsiNotFoundException {
        validationUtil.validate(updateKotaRequest);
        Provinsi provinsi = provinsiRepository.findById(updateKotaRequest.getProvinsiId()).orElseThrow(() -> new ProvinsiNotFoundException("Provinsi ID = ["+updateKotaRequest.getProvinsiId()+"] Not Found"));
        Kota kota = kotaRepository.findById(kotaId).orElseThrow(() -> new KotaNotFoundException("Kota ID = ["+kotaId+"] Not Found"));
        kota.setName(updateKotaRequest.getName());
        kota.setCode(updateKotaRequest.getCode());
        kota.setProvinsi(provinsi);
        kota.setUpdatedAt(LocalDateTime.now());

        kotaRepository.save(kota);
        return mapKotaToGetKotaResponse(kota);
    }

    @Override
    public void deleteKotaById(String kotaId) throws KotaNotFoundException {
        Kota kota = kotaRepository.findById(kotaId).orElseThrow(() -> new KotaNotFoundException("Kota ID = ["+kotaId+"] Not Found"));
        kotaRepository.delete(kota);
    }

    @Override
    public GetAllKotaResponse getAllKota(GetAllKotaRequest getAllKotaRequest) {
        validationUtil.validate(getAllKotaRequest);
        int pageNo = getAllKotaRequest.getPageNo();
        int pageSize = getAllKotaRequest.getPageSize();
        String sortBy = getAllKotaRequest.getSortBy();
        String sortDir = getAllKotaRequest.getSortDir();

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<Kota> kotaPage = kotaRepository.findAll(pageable);
        List<Kota> kotaList = kotaPage.getContent();
        List<GetKotaResponse> getKotaResponses = mapKotaToGetKotaResponseList(kotaList);

        GetAllKotaResponse getAllKotaResponse = new GetAllKotaResponse();
        getAllKotaResponse.setGetKotaResponses(getKotaResponses);
        getAllKotaResponse.setPageNo(kotaPage.getNumber());
        getAllKotaResponse.setPageSize(kotaPage.getSize());
        getAllKotaResponse.setTotalElements(kotaPage.getTotalElements());
        getAllKotaResponse.setTotalPages(kotaPage.getTotalPages());
        getAllKotaResponse.setLast(kotaPage.isLast());
        return getAllKotaResponse;
    }

    @Override
    public List<GetKotaResponse> getKotaByNameContainingIgnoreCase(String name) {
        List<Kota> kotaList = kotaRepository.findByNameContainingIgnoreCase(name);
        return mapKotaToGetKotaResponseList(kotaList);
    }

    @Override
    public List<GetKotaResponse> getKotaByProvinsiId(String provinsiId) {
        List<Kota> kotaList = kotaRepository.findByProvinsiId(provinsiId);
        return mapKotaToGetKotaResponseList(kotaList);
    }

    @Override
    public GetKotaResponse getKotaByCode(String code) throws KotaNotFoundException {
        Optional<Kota> kota = kotaRepository.findByCode(code);
        if (kota.isEmpty()) {
            throw new KotaNotFoundException("Kota Code : ["+code+"] is empty");
        }
        return mapKotaToGetKotaResponse(kota.get());
    }

    private List<GetKotaResponse> mapKotaToGetKotaResponseList(List<Kota> kotaList) {
        return kotaList.stream()
                .map((kota) -> {
                    GetKotaResponse getKotaResponse = new GetKotaResponse();
                    getKotaResponse.setId(kota.getId());
                    getKotaResponse.setName(kota.getName());
                    getKotaResponse.setCode(kota.getCode());
                    getKotaResponse.setCreatedAt(kota.getCreatedAt());
                    getKotaResponse.setUpdatedAt(kota.getUpdatedAt());
                    getKotaResponse.setProvinsi(kota.getProvinsi());
                    return getKotaResponse;
                })
                .collect(Collectors.toList())
                ;
    }

    private GetKotaResponse mapKotaToGetKotaResponse(Kota kota) {
        GetKotaResponse getKotaResponse = new GetKotaResponse();
        getKotaResponse.setId(kota.getId());
        getKotaResponse.setName(kota.getName());
        getKotaResponse.setCode(kota.getCode());
        getKotaResponse.setCreatedAt(kota.getCreatedAt());
        getKotaResponse.setUpdatedAt(kota.getUpdatedAt());
        getKotaResponse.setProvinsi(kota.getProvinsi());
        return getKotaResponse;
    }
}
