package com.banking.management.service.wilayah;

import com.banking.management.dto.wilayah.kota.*;
import com.banking.management.exception.KotaNotFoundException;
import com.banking.management.exception.ProvinsiNotFoundException;

import java.util.List;

public interface KotaService {

    GetKotaResponse createKota(String provinsiId, CreateKotaRequest createKotaRequest) throws ProvinsiNotFoundException;

    GetKotaResponse getKotaById(String kotaId) throws KotaNotFoundException;

    GetKotaResponse updateKota(String kotaId, UpdateKotaRequest updateKotaRequest) throws KotaNotFoundException, ProvinsiNotFoundException;

    void deleteKotaById(String kotaId) throws KotaNotFoundException;

    GetAllKotaResponse getAllKota(GetAllKotaRequest getAllKotaRequest);

    GetKotaResponse getKotaByName(String name) throws KotaNotFoundException;

    GetKotaResponse getKotaByCode(String code) throws KotaNotFoundException;

    List<GetKotaResponse> getKotaByNameContaining(String name);

    List<GetKotaResponse> getKotaByProvinsiId(String provinsiId);
}
