package com.banking.management.entity.wilayah;

import com.banking.management.entity.BaseEntity;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "kota", uniqueConstraints = {
        @UniqueConstraint(name = "kota_code_unique", columnNames = "code")
})
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@SQLDelete(sql = "UPDATE kota SET status_record = 'INACTIVE' WHERE id = ?")
@Where(clause = "status_record = 'ACTIVE'")
public class Kota extends BaseEntity {


    @Column(name = "code", nullable = false, length = 20)
    private String code;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @ToString.Exclude
    @FieldNameConstants.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_provinsi", foreignKey = @ForeignKey(name = "fk_kota_provinsi"))
    private Provinsi provinsi;
}
