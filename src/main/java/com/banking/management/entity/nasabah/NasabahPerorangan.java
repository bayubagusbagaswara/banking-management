package com.banking.management.entity.nasabah;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "nasabah_perorangan")
@PrimaryKeyJoinColumn(name = "id_nasabah", foreignKey = @ForeignKey(name = "fk_nasabah_perorangan_nasabah"))
public class NasabahPerorangan extends Nasabah {

    @Column(name = "nama_ibu_kandung", nullable = false, length = 100)
    private String namaIbuKandung;

    @Column(name = "tanggal_lahir", nullable = false)
    private LocalDate tanggalLahir;

    @Column(name = "nomor_telephone", nullable = false, length = 30)
    private String noTelp;
}
