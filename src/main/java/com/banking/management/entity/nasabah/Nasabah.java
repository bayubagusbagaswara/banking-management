package com.banking.management.entity.nasabah;

import com.banking.management.entity.StatusRecord;
import com.banking.management.entity.wilayah.Kelurahan;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "nasabah", uniqueConstraints = {
        @UniqueConstraint(name = "nasabah_cif_unique", columnNames = "cif"),
        @UniqueConstraint(name = "nasabah_nomor_identitas_unique", columnNames = "nomor_identitas")
})
@Inheritance(strategy = InheritanceType.JOINED)
public class Nasabah {

    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Column(nullable = false, length = 64)
    private String id;

    @Column(name = "nama_kepemilikan", nullable = false, length = 100)
    private String namaKepemilikan;

    @Column(name = "cif", nullable = false, length = 30)
    private String cif;

    @Column(name = "nomor_identitas", nullable = false, length = 30)
    private String noIdentitas;

    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status_record", nullable = false)
    private StatusRecord statusRecord = StatusRecord.ACTIVE;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @FieldNameConstants.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_kelurahan", foreignKey = @ForeignKey(name = "fk_nasabah_kelurahan"))
    private Kelurahan kelurahan;
}
