package com.banking.management.entity.nasabah;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "nasabah_badan_usaha")
@PrimaryKeyJoinColumn(name = "id_nasabah", foreignKey = @ForeignKey(name = "fk_nasabah_badan_usaha_nasabah"))
public class NasabahBadanUsaha extends Nasabah {

    @Column(name = "nomor_siup", nullable = false, length = 50)
    private String noSiup;

    @Column(name = "nomor_akta_terakhir", nullable = false, length = 50)
    private String noAktaTerakhir;

    @Column(name = "nomor_telephone", nullable = false, length = 30)
    private String noTelp;
}
