package com.banking.management.entity.tabungan;

import com.banking.management.entity.BaseEntity;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "transaksi_tabungan")
@SQLDelete(sql = "UPDATE transaksi_tabungan SET status_record = 'INACTIVE' WHERE id = ?")
@Where(clause = "status_record = 'ACTIVE'")
public class TransaksiTabungan extends BaseEntity {

    @Column(name = "tanggal")
    private LocalDateTime tanggal;

    @Column(name = "debit")
    private BigDecimal debit;

    @Column(name = "kredit")
    private BigDecimal kredit;

    @Column(name = "saldo")
    private BigDecimal saldo;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @FieldNameConstants.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rekening_tabungan", foreignKey = @ForeignKey(name = "fk_transaksi_tabungan_rekening_tabungan"))
    private RekeningTabungan rekeningTabungan;
}
