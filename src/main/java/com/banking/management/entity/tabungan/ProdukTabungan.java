package com.banking.management.entity.tabungan;

import com.banking.management.entity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "produk_tabungan")
@SQLDelete(sql = "UPDATE produk_tabungan SET status_record = 'INACTIVE' WHERE id = ?")
@Where(clause = "status_record = 'ACTIVE'")
public class ProdukTabungan extends BaseEntity {

    @Column(name = "nama")
    private String nama;

    @Column(name = "min_setoran_awal")
    private BigDecimal minSetoranAwal;

    @Column(name = "suku_bunga")
    private Float sukuBunga;

    @Column(name = "biaya_admin")
    private BigDecimal biayaAdmin;
}
