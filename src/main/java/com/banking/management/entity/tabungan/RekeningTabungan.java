package com.banking.management.entity.tabungan;

import com.banking.management.entity.BaseEntity;
import com.banking.management.entity.nasabah.Nasabah;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "rekening_tabungan")
@SQLDelete(sql = "UPDATE rekening_tabungan SET status_record = 'INACTIVE' WHERE id = ?")
@Where(clause = "status_record = 'ACTIVE'")
public class RekeningTabungan extends BaseEntity {

    @ToString.Exclude
    @FieldNameConstants.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_produk_tabungan", foreignKey = @ForeignKey(name = "fk_rekening_tabungan_produk_tabungan"))
    private ProdukTabungan produkTabungan;

    @ToString.Exclude
    @FieldNameConstants.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nasabah", foreignKey = @ForeignKey(name = "fk_rekening_tabungan_nasabah"))
    private Nasabah nasabah;

    @Column(name = "suku_bunga")
    private Float sukuBunga;

    @Column(name = "biaya_admin")
    private BigDecimal biayaAdmin;

    @Column(name = "saldo")
    private BigDecimal saldo;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @FieldNameConstants.Exclude
    @OneToMany(mappedBy = "rekeningTabungan")
    private List<TransaksiTabungan> transaksiTabunganList = new ArrayList<>();
}
